**Medication Warehouse Application** (by Umarova Sarvinoz)

**Technical Documentation**

**General Overview**

The Medication Warehouse Application is a Java-based application specifically designed to manage a database of medications. The primary objective of this application is to maintain a record of medications, encapsulating details such as name, category, country, amount in a package, expiration date, and price.

**Application Architecture**

The application is structured following the Model-View-Controller (MVC) architectural pattern, distinctly separating different layers:

1. **Model Layer**: This layer deals with the data representation of the application, primarily focusing on the `Medication` entity.

2. **View Layer**: Solely responsible for presenting information to the user. It displays data devoid of any application logic.

3. **Controller Layer**: Manages the logic of accepting user inputs and updating the view, acting as a bridge between the View and Model layers.

4. **Service Layer**: Contains business logic, managing data processing and operations.

5. **Main Class**: The entry point of the application, managing user interfaces and input.

**Detailed Component Overview**

1)EntryPoint: This is the main class that drives the application. It contains the main method which manages the user’s interaction with the system. The main method first authenticates the user and then enters a loop where it displays a menu of actions to the user and handles the user’s choice of action. The printAllDataFromTheFile method is used to display all the medications in the database.

2)Admin: This class represents the administrator role in your system. It has methods to add and delete medications. The addMedication method takes a string of product details, validates the data, and if valid, adds a new medication to the database. The deleteMedication method takes a product name and removes the product from the database.

3)AuthService: This class is responsible for authenticating users. It loads user credentials from a CSV file and checks if a user-provided login and password match the stored credentials. The loadUsersFromCsv method is used to load user credentials from the CSV file. The authenticate method takes a login and password and checks if they match the stored credentials.

4)User: This class represents a user in your system. It has methods to view medications by various attributes such as name, category, country, amount in a package, expiration date, and price.

5)MedicationDao: This is an interface that defines the operations that can be performed on the medication data. It includes methods to retrieve all medications and to retrieve medications by various attributes. It also includes methods to add and delete medications.

6)MedicationCsvBasedDaoImpl: This class implements the MedicationDao interface. It provides the functionality to perform operations on the medication data stored in a CSV file. The fetchDataFromCsvFile method is used to read data from the CSV file. The transformStringDataIntoDtoBasedCollection method is used to convert the read data into a list of Medication objects. The removeHeaderDataFromPureDataSet method is used to remove the header row from the data.

7)Medication: This class represents a medication. It has fields for various attributes of a medication and getter and setter methods for these fields.

8)MedicationException: This class represents an exception that can be thrown when there is an error related to a medication.

9)CustomStringToMedicationDataRowMapper: This class is used to convert a string array of medication data into a Medication object.

10)MedicationService: This is an interface that defines the services that can be performed on the medication data. It includes methods to retrieve all medications and to retrieve medications by various attributes.

11)MedicationServiceImpl: This class implements the MedicationService interface. It provides the functionality to retrieve medications by various attributes.

**Key Components**

**Model - Medication Entity**

The Medication class is at the heart of the model layer, representing a medication record with attributes such as name, category, country, amount in a package, expiration date, and price.

**Service Layer**

The service layer abstracts the business logic of the application. It includes two primary services:

- MedicationService: Offers functionalities for general users like retrieving medications based on specific criteria.
- AdminService: Extends the functionalities of MedicationService, adding capabilities for administrators to add, update, and delete medication records.

**Data Access Object (DAO)**

The DAO layer enables data persistence and retrieval from a CSV file. It includes:

- MedicationDaoImpl: Manages data retrieval for general users.
- MedicationDaoImplAdmin: Extends MedicationDaoImpl, providing data manipulation functionalities for administrators.

**View Layer**

The view layer, implemented through the EntryPoint class, is tasked with presenting medication data. It displays a menu of actions to the user and handles the user's choice of action.

**Controller Layer**

This layer includes:
- User: Represents a user in the system. It has methods to view medications by various attributes.
- Admin: Represents the administrator role in the system. It has methods to add and delete medications.

**User Authentication**

Managed by the AuthService class, it authenticates users by verifying credentials against a separate CSV file.

**Main Class**

The EntryPoint class serves as the application's entry point, orchestrating user interaction, command processing, and initialization of components.

**Application Workflow**

1. Initialization: Initiated by EntryPoint, the application sets up services, controllers, and views.
2. User Interaction: Users interact through the command-line interface, issuing commands processed by EntryPoint.
3. Command Processing: User commands are routed to the appropriate controller for processing.
4. Service Execution: The service layer executes the business logic and interacts with the DAO for data persistence or retrieval.
5. View Updates: The view layer reflects changes in the data, ensuring users see the most recent information.

**Technical Considerations**

- The application ensures immediate reflection of administrative changes in user views.
- Designed specifically for medication management, with categories being a crucial attribute of a medication.

**Conclusion**

The Medication Warehouse Application is a robust tool for medication management, characterized by its modular architecture, clear separation of responsibilities, and emphasis on data integrity and user experience.

**Instructions on how the application works**

1. The application starts by asking the client to log in as an Admin or User.
2. If the client chooses to log in as an Admin, they are asked to enter their login and password. If the authentication is successful, the client is granted admin privileges.
3. If the client chooses to log in as a User, they are granted user privileges.
4. The client is then presented with a menu of actions. The available actions depend on whether the user is an Admin or a User.
5. Admins can view all products, search for products by various attributes, add products, delete products, and exit the program.
6. Users can view all products and search for products by various attributes.
7. The client selects an action by entering the corresponding number.
8. If the client chooses to view all products, the application retrieves all the medications from the database and displays them.
9. If the client chooses to search for products, they are asked to enter the search criteria. The application then retrieves the matching medications from the database and displays them.
10. If the client chooses to add a product (Admin only), they are asked to enter the product details. The application then adds the product to the database.
11. If the client chooses to delete a product (Admin only), they are asked to enter the name of the product. The application then deletes the product from the database.
12. If the client chooses to exit the program, the application ends.
13. If the client enters an invalid choice, they are asked to enter a valid choice.
