package com.epam.training.controller;

import java.nio.file.*;
import java.io.*;
import java.util.*;

public class AuthService {
    private Map<String, String> users;

    public AuthService(String usersFilePath) {
        users = new HashMap<>();
        loadUsersFromCsv(usersFilePath);
    }

    private void loadUsersFromCsv(String usersFilePath) {
        Path pathToFile = Paths.get(usersFilePath);
        try (BufferedReader br = Files.newBufferedReader(pathToFile)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                users.put(values[0], values[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean authenticate(String login, String password) {
        String correctPassword = users.get(login);
        return correctPassword != null && correctPassword.equals(password);
    }
}

