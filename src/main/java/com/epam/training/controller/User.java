package com.epam.training.controller;

import com.epam.training.dto.Medication;
import com.epam.training.service.MedicationService;

import java.util.List;

public class User {
    private MedicationService medicationService;

    public User(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    public List<Medication> viewAllMedications() {
        return medicationService.retrieveAllMedications();
    }

    public List<Medication> viewMedicationsByName(String name) {
        return medicationService.retrieveMedicationsByName(name);
    }

    public List<Medication> viewMedicationsByCategory(String category) {
        return medicationService.retrieveMedicationsByCategory(category);
    }

    public List<Medication> viewMedicationsByCountry(String country) {
        return medicationService.retrieveMedicationsByCountry(country);
    }

    public List<Medication> viewMedicationsByAmountInAPackage(int amountInAPackage) {
        return medicationService.retrieveMedicationsByAmountInAPackage(amountInAPackage);
    }

    public List<Medication> viewMedicationsByExpirationDate(int expirationDate) {
        return medicationService.retrieveMedicationsByExpirationDate(expirationDate);
    }

    public List<Medication> viewMedicationsByPrice(int price) {
        return medicationService.retrieveMedicationsByPrice(price);
    }
}


