package com.epam.training.controller;

import com.epam.training.dao.MedicationDao;

public class Admin {
    private static MedicationDao medicationDao;

    public Admin(MedicationDao medicationDao) {
        Admin.medicationDao = medicationDao;
    }


    public static void addMedication(String productDetails) {
        if (medicationDao.isDataValid(productDetails)) {
            medicationDao.addMedication(productDetails);
            System.out.println("Medication added");
        } else {
            System.out.println("You have not entered complete data");
        }
    }

    public static void deleteMedication(String name) {
        boolean isDeleted = medicationDao.deleteMedication(name);
        if (isDeleted) {
            System.out.println("Medication deleted");
        }
    }
}

