package com.epam.training.service;

import com.epam.training.dto.Medication;

import java.util.List;

public interface MedicationService {


    List<Medication> retrieveAllMedications();

    List<Medication> retrieveMedicationsByName(String name);

    List<Medication> retrieveMedicationsByCategory(String category);

    List<Medication> retrieveMedicationsByCountry(String country);

    List<Medication> retrieveMedicationsByAmountInAPackage(int amountInAPackage);

    List<Medication> retrieveMedicationsByExpirationDate(int expirationDate);

    List<Medication> retrieveMedicationsByPrice(int price);
}
