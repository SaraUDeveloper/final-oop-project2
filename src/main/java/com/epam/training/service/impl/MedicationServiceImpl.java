package com.epam.training.service.impl;

import com.epam.training.dao.MedicationDao;
import com.epam.training.dto.Medication;
import com.epam.training.service.MedicationService;

import java.util.ArrayList;
import java.util.List;

public class MedicationServiceImpl implements MedicationService {

    private MedicationDao dao;

    public MedicationServiceImpl(MedicationDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Medication> retrieveAllMedications() {
        return dao.retrieveAllMedicationsFromDataSource();
    }

    @Override
    public List<Medication> retrieveMedicationsByName(String name) {
        return dao.retrieveMedicationsByName(name);
    }

    @Override
    public List<Medication> retrieveMedicationsByCategory(String category) {
        return dao.retrieveMedicationsByCategory(category);
    }

    @Override
    public List<Medication> retrieveMedicationsByCountry(String country) {
        return dao.retrieveMedicationsByCountry(country);
    }

    @Override
    public List<Medication> retrieveMedicationsByAmountInAPackage(int amountInAPackage) {
        return dao.retrieveMedicationsByAmountInAPackage(amountInAPackage);
    }

    @Override
    public List<Medication> retrieveMedicationsByExpirationDate(int expirationDate) {
        return dao.retrieveMedicationsByExpirationDate(String.valueOf(expirationDate));
    }

    @Override
    public List<Medication> retrieveMedicationsByPrice(int price) {
        return dao.retrieveMedicationsByPrice(price);
    }
}

