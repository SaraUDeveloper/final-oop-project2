package com.epam.training.rowmapper;

import com.epam.training.dto.Medication;


public class CustomStringToMedicationDataRowMapper {

    public Medication mapRaw(String[] row) {
        Medication medication = new Medication();
        medication.setName(row[0]);
        medication.setCategory(row[1]);
        medication.setCountry(row[2]);
        medication.setAmountInAPackage(Integer.parseInt(row[3]));
        medication.setExpirationDate(Integer.parseInt(row[4]));
        medication.setPrice(Integer.parseInt(row[5]));

        return medication;
    }

}
