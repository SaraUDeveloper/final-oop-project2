package com.epam.training.dto;

import java.util.Objects;

public class Medication {

    // name,category,country,amount_in_a_package,expiration_date,price

    private String name;
    private String category;
    private String country;
    private int amountInAPackage;
    private int expirationDate;
    private int price;

    public Medication(String name, String category, String country, int amountInAPackage,  int expirationDate, int price) {
        this.name = name;
        this.category = category;
        this.country = country;
        this.amountInAPackage = amountInAPackage;
        this.expirationDate = expirationDate;
        this.price = price;
    }

    public Medication() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAmountInAPackage() {
        return amountInAPackage;
    }

    public void setAmountInAPackage(int amountInAPackage) {
        this.amountInAPackage = amountInAPackage;
    }

    public int getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(int expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", country='" + country + '\'' +
                ", amountInAPackage=" + amountInAPackage +
                ", expirationDate=" + expirationDate +
                ", price=" + price +
                '}';
    }
}
