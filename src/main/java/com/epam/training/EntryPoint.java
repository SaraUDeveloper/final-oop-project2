package com.epam.training;

import com.epam.training.controller.Admin;
import com.epam.training.controller.AuthService;
import com.epam.training.controller.User;
import com.epam.training.dao.MedicationDao;
import com.epam.training.dao.impl.MedicationCsvBasedDaoImpl;
import com.epam.training.dto.Medication;
import com.epam.training.service.MedicationService;
import com.epam.training.service.impl.MedicationServiceImpl;

import java.util.List;
import java.util.Scanner;


public class EntryPoint {
    public static void main(String[] args) {
        MedicationDao dao = new MedicationCsvBasedDaoImpl();
        MedicationService service = new MedicationServiceImpl(dao);
        AuthService authService = new AuthService("src/main/resources/csv/users.csv");
        Object user = null;

        Scanner scanner = new Scanner(System.in);

        while (user == null) {
            System.out.println("_______________________________________________");
            System.out.println("*     Welcome to  Medication Warehouse! ❤    *");
            System.out.println("-----------------------------------------------");
            System.out.println("*    Do you want to login as Admin or User?   *");
            System.out.println("-----------------------------------------------");
            System.out.println("*        A - Admin\uD83D\uDE0E  or   B - User\uD83D\uDE42         *");
            System.out.println("_______________________________________________");

            String role = scanner.nextLine();

            if (role.equalsIgnoreCase("A")) {
                System.out.println("Enter login:");
                String login = scanner.nextLine();

                System.out.println("Enter password:");
                String password = scanner.nextLine();

                if (!authService.authenticate(login, password)) {
                    System.out.println("Invalid login or password");
                    continue;
                }

                user = new Admin(new MedicationCsvBasedDaoImpl());
            } else if (role.equalsIgnoreCase("B")) {
                user = new User(new MedicationServiceImpl(new MedicationCsvBasedDaoImpl()));
            } else {
                System.out.println("Wrong choice");
            }
        }

        while (true) {
            System.out.println("_________________________________________________");
            System.out.println("| Enter the desired number to select an action: |");
            System.out.println("-------------------------------------------------");
            System.out.println("|    1 - view all products                      |");
            System.out.println("|    2 - search product by name                 |");
            System.out.println("|    3 - search product by category             |");
            System.out.println("|    4 - search product by country              |");
            System.out.println("|    5 - search product by amount in a package  |");
            System.out.println("|    6 - search product by expiration date      |");
            System.out.println("|    7 - search product by price                |");
            System.out.println("|    8 - add product(admin only)                |");
            System.out.println("|    9 - delete product(admin only)             |");
            System.out.println("|    10 - exit program                          |");
            System.out.println("-------------------------------------------------");

            int choice = scanner.nextInt();
            scanner.nextLine();

            if (choice >= 8 && choice <= 10 && !(user instanceof Admin)) {
                System.out.println("Only admins can perform this action.");
                System.exit(0);
            }

            if (choice == 1) {
                // View all products
                printAllDataFromTheFile(service);
            } else if (choice == 2) {
                // Search product by name
                System.out.print("Enter the name of the product: ");
                String name = scanner.nextLine();
                List<Medication> surveyData = service.retrieveMedicationsByName(name);
                for (Medication sdItem : surveyData) {
                    System.out.println(sdItem);
                }
            } else if (choice == 3) {
                // Search product by category
                System.out.print("Enter the category of the product: ");
                String category = scanner.nextLine();
                List<Medication> surveyData = service.retrieveMedicationsByCategory(category);
                for (Medication sdItem : surveyData) {
                    System.out.println(sdItem);
                }
            } else if (choice == 4) {
                // Search product by country
                System.out.print("Enter the country of the product: ");
                String country = scanner.nextLine();
                List<Medication> surveyData = service.retrieveMedicationsByCountry(country);
                for (Medication sdItem : surveyData) {
                    System.out.println(sdItem);
                }
            } else if (choice == 5) {
                // Search product by quantity
                System.out.print("Enter the amount in a package of the product: ");
                int amountInAPackage = scanner.nextInt();
                scanner.nextLine();
                List<Medication> surveyData = service.retrieveMedicationsByAmountInAPackage(amountInAPackage);
                for (Medication sdItem : surveyData) {
                    System.out.println(sdItem);
                }
            } else if (choice == 6) {
                // Search product by expiration date
                System.out.print("Enter the expiration date of the product (years): ");
                String expirationDate = scanner.nextLine();
                List<Medication> surveyData = service.retrieveMedicationsByExpirationDate(Integer.parseInt(expirationDate));
                for (Medication sdItem : surveyData) {
                    System.out.println(sdItem);
                }
            } else if (choice == 7) {
                // Search product by price
                System.out.print("Enter the price of the product: ");
                int price = scanner.nextInt();
                scanner.nextLine();
                List<Medication> surveyData = service.retrieveMedicationsByPrice(price);
                for (Medication sdItem : surveyData) {
                    System.out.println(sdItem);
                }
            } else if (choice == 8) {
                // Add product
                System.out.println("Enter the details of the product (name,category,country,amount_in_a_package,expiration_date,price):");
                String productDetails = scanner.nextLine();
                Admin.addMedication(productDetails);
            } else if (choice == 9) {
                // Delete product
                System.out.println("Enter the name of the product to delete:");
                String productName = scanner.nextLine();
                Admin.deleteMedication(productName);
            } else if (choice == 10) {
                // Exit program
                return;
            } else {
                System.out.println("Invalid choice");
            }
        }
    }


    private static void printAllDataFromTheFile(MedicationService service) {
        List<Medication> surveyData = service.retrieveAllMedications();
        for (Medication sdItem : surveyData) {
            System.out.println(sdItem);
        }
    }
}
