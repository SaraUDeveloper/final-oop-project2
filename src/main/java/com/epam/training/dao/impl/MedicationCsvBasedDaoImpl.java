package com.epam.training.dao.impl;

import com.epam.training.dao.MedicationDao;
import com.epam.training.dto.Medication;
import com.epam.training.rowmapper.CustomStringToMedicationDataRowMapper;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MedicationCsvBasedDaoImpl implements MedicationDao {

    private static final String PATH_TO_DATA_SOURCE = "src/main/resources/csv/medications.csv";
    private List<Medication> medications;

    public MedicationCsvBasedDaoImpl() {
        medications = retrieveAllMedicationsFromDataSource();
    }

    @Override
    public List<Medication> retrieveAllMedicationsFromDataSource() {

        List<String[]> allData = fetchDataFromCsvFile();

        removeHeaderDataFromPureDataSet(allData);

        List<Medication> sDataResulted = transformStringDataIntoDtoBasedCollection(allData);
        return sDataResulted;
    }

    public List<Medication> retrieveMedicationsByName(String name) {
        List<String[]> allData = fetchDataFromCsvFile();
        removeHeaderDataFromPureDataSet(allData);
        List<Medication> sDataResulted = new ArrayList<>();
        for (String[] row : allData) {
            if (row[0].equals(name)) {
                CustomStringToMedicationDataRowMapper rowMapper = new CustomStringToMedicationDataRowMapper();
                Medication sd = rowMapper.mapRaw(row);
                sDataResulted.add(sd);
            }
        }
        return sDataResulted;
    }

    public List<Medication> retrieveMedicationsByCategory(String category) {
        List<String[]> allData = fetchDataFromCsvFile();
        removeHeaderDataFromPureDataSet(allData);
        List<Medication> sDataResulted = new ArrayList<>();
        for (String[] row : allData) {
            if (row[1].equals(category)) {
                CustomStringToMedicationDataRowMapper rowMapper = new CustomStringToMedicationDataRowMapper();
                Medication sd = rowMapper.mapRaw(row);
                sDataResulted.add(sd);
            }
        }
        return sDataResulted;
    }

    public List<Medication> retrieveMedicationsByCountry(String country) {
        List<String[]> allData = fetchDataFromCsvFile();
        removeHeaderDataFromPureDataSet(allData);
        List<Medication> sDataResulted = new ArrayList<>();
        for (String[] row : allData) {
            if (row[2].equals(country)) {
                CustomStringToMedicationDataRowMapper rowMapper = new CustomStringToMedicationDataRowMapper();
                Medication sd = rowMapper.mapRaw(row);
                sDataResulted.add(sd);
            }
        }
        return sDataResulted;
    }

    public List<Medication> retrieveMedicationsByAmountInAPackage(int amountInAPackage) {
        List<String[]> allData = fetchDataFromCsvFile();
        removeHeaderDataFromPureDataSet(allData);
        List<Medication> sDataResulted = new ArrayList<>();
        for (String[] row : allData) {
            if (Integer.parseInt(row[3]) == amountInAPackage) {
                CustomStringToMedicationDataRowMapper rowMapper = new CustomStringToMedicationDataRowMapper();
                Medication sd = rowMapper.mapRaw(row);
                sDataResulted.add(sd);
            }
        }
        return sDataResulted;
    }

    public List<Medication> retrieveMedicationsByExpirationDate(String expirationDate) {
        List<String[]> allData = fetchDataFromCsvFile();
        removeHeaderDataFromPureDataSet(allData);
        List<Medication> sDataResulted = new ArrayList<>();
        for (String[] row : allData) {
            if (row[4].equals(expirationDate)) {
                CustomStringToMedicationDataRowMapper rowMapper = new CustomStringToMedicationDataRowMapper();
                Medication sd = rowMapper.mapRaw(row);
                sDataResulted.add(sd);
            }
        }
        return sDataResulted;
    }

    public List<Medication> retrieveMedicationsByPrice(int price) {
        List<String[]> allData = fetchDataFromCsvFile();
        removeHeaderDataFromPureDataSet(allData);
        List<Medication> sDataResulted = new ArrayList<>();
        for (String[] row : allData) {
            if (Integer.parseInt(row[5]) == price) {
                CustomStringToMedicationDataRowMapper rowMapper = new CustomStringToMedicationDataRowMapper();
                Medication sd = rowMapper.mapRaw(row);
                sDataResulted.add(sd);
            }
        }
        return sDataResulted;
    }

    @Override
    public void addMedication(String productDetails) {
        String[] details = productDetails.split(",");
        if (details.length < 6) {
            return;
        }
        Medication newMedication = new Medication(details[0], details[1], details[2], Integer.parseInt(details[3]), Integer.parseInt(details[4]), Integer.parseInt(details[5]));
        medications.add(newMedication);
        writeMedicationsToCsv();
    }

    public boolean isDataValid(String productDetails) {
        String[] details = productDetails.split(",");
        return details.length >= 6;
    }


    @Override
    public boolean deleteMedication(String name) {
        // Check if a medication is on the list
        boolean exists = medications.stream().anyMatch(medication -> medication.getName().equals(name));
        if (!exists) {
            System.out.println("There's no such drug in the database");
            return false;
        }
        // Remove a medication from the list
        medications.removeIf(medication -> medication.getName().equals(name));
        // Rewrite CSV file
        writeMedicationsToCsv();
        return true;
    }

    private void writeMedicationsToCsv() {
        try (FileWriter writer = new FileWriter(PATH_TO_DATA_SOURCE)) {
            writer.write("name,category,country,amount_in_a_package,expiration_date,price\n");
            for (Medication medication : medications) {
                writer.write(medication.getName() + "," + medication.getCategory() + "," + medication.getCountry() + "," + medication.getAmountInAPackage() + "," + medication.getExpirationDate() + "," + medication.getPrice() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static List<String[]> fetchDataFromCsvFile() {
        FileReader filereader = null;
        try {
            filereader = new FileReader(PATH_TO_DATA_SOURCE);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        CSVParser parser = new CSVParserBuilder()/*.withSeparator(rawSeparator)*/.build();
        CSVReader csvReader = new CSVReaderBuilder(filereader)
                .withCSVParser(parser)
                .build();

        List<String[]> allData;
        try {
            allData = csvReader.readAll();
        } catch (IOException | CsvException e) {
            throw new RuntimeException(e);
        }
        return allData;
    }

    private List<Medication> transformStringDataIntoDtoBasedCollection(List<String[]> allData) {
        CustomStringToMedicationDataRowMapper rowMapper =new CustomStringToMedicationDataRowMapper();
        List<Medication> result = new ArrayList<>();

//        for (String[] row : allData.subList(0,5)) {
        for (String[] row : allData) {
            Medication sd = rowMapper.mapRaw(row);

            result.add(sd);
        }
        return result;
    }

    private void removeHeaderDataFromPureDataSet(List<String[]> allData) {
        allData.remove(0);
    }
}
