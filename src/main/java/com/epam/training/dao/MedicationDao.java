package com.epam.training.dao;

import com.epam.training.dto.Medication;

import java.util.List;

public interface MedicationDao {

    List<Medication> retrieveAllMedicationsFromDataSource();

    List<Medication> retrieveMedicationsByName(String name);

    List<Medication> retrieveMedicationsByCategory(String category);

    List<Medication> retrieveMedicationsByCountry(String country);

    List<Medication> retrieveMedicationsByAmountInAPackage(int amountInAPackage);

    List<Medication> retrieveMedicationsByExpirationDate(String expirationDate);

    List<Medication> retrieveMedicationsByPrice(int price);

    void addMedication(String medication);

    boolean deleteMedication(String name);

    boolean isDataValid(String productDetails);

}

